The  streams-environment
========================

The streams-environment is a package providing the basic streams
library jars and some user-friendly commands as a Debian/RPM
package.


Building
--------

The streams-environment packages can be built using the provided
Makefile. Simply run:

     # make package deb

will create the Debian package in the `releases` directory.
