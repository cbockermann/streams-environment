VERSION=0.9.5-SNAPSHOT
REVISION=0
NAME=streams
BUILD=.build_tmp
DIST=jwall-devel
DEB_FILE=streams-environment-${VERSION}-${REVISION}.deb
RPM_FILE=streams-environment-${VERSION}-${REVISION}.noarch.rpm
RELEASE_DIR=releases
RPMBUILD=$(PWD)/.rpmbuild
ARCH=noarch

clean:
	@echo "Cleaning generated code..."
	@mvn clean
	@echo "Removing release directory..."
	@rm -rf releases


package:
	echo "Preparing package..."
	mvn package

pre-package:
	echo "Preparing packages build in ${BUILD}"
	mkdir -p ${BUILD}
	mkdir -p ${BUILD}/opt/streams/lib
	cp -a dist/opt ${BUILD}/
#	cp stream-api/target/stream-api-${VERSION}.jar ${BUILD}/opt/streams/lib
	cp target/dependency/*.jar ${BUILD}/opt/streams/lib/ 


deb:	pre-package
	rm -rf ${RELEASE_DIR}
	mkdir -p ${RELEASE_DIR}
	mkdir -p ${BUILD}/DEBIAN
	mkdir -p ${BUILD}/opt/streams/lib
	cp dist/DEBIAN/* ${BUILD}/DEBIAN/
	cat dist/DEBIAN/control | sed -e 's/Version:.*/Version: ${VERSION}-${REVISION}/' > ${BUILD}/DEBIAN/control
	chmod 755 ${BUILD}/DEBIAN/p*
	cd ${BUILD} && find opt -type f -exec md5sum {} \; > DEBIAN/md5sums && cd ..
	dpkg -b ${BUILD} ${RELEASE_DIR}/${DEB_FILE}
	md5sum ${RELEASE_DIR}/${DEB_FILE} > ${RELEASE_DIR}/${DEB_FILE}.md5
	rm -rf ${BUILD}
#	debsigs --sign=origin --default-key=C5C3953C ${RELEASE_DIR}/${DEB_FILE}

release-deb:
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian includedeb ${DIST} ${RELEASE_DIR}/${DEB_FILE}


unrelease-deb:
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian remove ${DIST} auditconsole
